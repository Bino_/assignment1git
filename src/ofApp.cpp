#include "ofApp.h"
/*
	By: Andrew Neel
	Last updated: 1/21/20
	a ball that bounces from one side of the window to the other
*/
//--------------------------------------------------------------
void ofApp::setup(){
	flip.addListener(this, &ofApp::flipPressed);

	gui.setup();
	gui.add(radius.setup("radius", 50, 10, 200));
	gui.add(flip.setup("turn around"));
	gui.add(backgroudColor.setup("background color", ofColor(200, 100, 100), ofColor(0, 0), ofColor(255, 255)));
	gui.add(circleColor.setup("circle color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));
	
	//decides if ball bounces left-right or tob-bottom
	int direction = ofRandom(10);
	//random starting position
	x = ofRandom(1000);
	y = ofRandom(1000);
	
	if (direction % 2 == 0) {
		xSpeed = ofRandom(-10, 10);
		ySpeed = 0;
	}
	else {
		xSpeed = 0;
		ySpeed = ofRandom(-10, 10);
	}
	
}

//--------------------------------------------------------------
void ofApp::update(){
	//increases position
	x += xSpeed;
	y += ySpeed;
	//if ball hits the window border flips the speed sending the ball in the oposite direction
	if (x > ofGetWindowWidth()|| x < 0) {
		xSpeed *= -1;
	}
	if (y > ofGetWindowHeight() || y < 0) {
		ySpeed *= -1;
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofBackground(backgroudColor);
	ofSetColor(circleColor);
	
	ofDrawCircle(x, y, radius);
	gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//changes direction ball is moving based on what key is pressed
	if (key == 's' || key == 'S') {
		xSpeed = 0;
		ySpeed = ofRandom(1, 10);
	}

	if (key == 'w' || key == 'W') {
		xSpeed = 0;
		ySpeed = ofRandom(-10, -1);
	}

	if (key == 'd' || key == 'D') {
		xSpeed = ofRandom(1, 10);
		ySpeed = 0;
	}

	if (key == 'a' || key == 'A') {
		xSpeed = ofRandom(-10, -1);
		ySpeed = 0;
	}
}

//--------------------------------------------------------------
void ofApp::flipPressed(){
	xSpeed *= -1;
	ySpeed *= -1;
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
